Writing subtitle files
======================

You can write subtitles in any text file.

You can make *labeled* or *simple* subtitles.

Difference is that with *labeled subtitles* you can print subtitle row **by its label in meta column** and with *simple subtitles* you can print content **only by moving cursor down**.

Labeled subtitles
^^^^^^^^^^^^^^^^^

For example *Microsoft Excel*, *LibreOffice Calc* or *Google Sheets* supports columns by design and it will be best for you to write labeled subtitle files in some sheets manipulating software like that and then export it as .csv or .tsv file.

Make sure you use same delimiter when exporting file and on SubtitleManager in Unity project.

Let's say the table below is a part of your subtitle file:

========  ===============
A         B
========  ===============
...
oh        begin niceness
this is
really    rly is here
really
nice
yeah      end
...
========  ===============

| **Column A** - Where your subtitles are stored
| **Column B** - Where meta data about subtitles are stored

To print the part of file between ``begin`` and ``end`` (also called conversations) you can use function ``yourSubtitleManagerReference.Print ("niceness", true);`` or to print single line of your choice (in this case 'really'), use ``yourSubtitleManagerReference.Print ("rly is here", false);``.

See more on :ref:`print-f1`.

Simple subtitles
^^^^^^^^^^^^^^^^

You can write simple subtitles in .txt file. Only thing you have to worry about is that rows are not too long or characters will not fit in your UI in Unity3D project. Let's say example below is our subtitles.txt

| oh
| this is
| really
| really
| nice
| yeah

| can you
| dig it?

You can leave row empty for end of section and use ``yourSubtitleManagerReference.PrintUntilEmptyRow()``. Use ``yourSubtitleManagerReference.Print(2)`` to print 2 rows. After you call any print function *cursor* points to next row.

For example, if your first function call on example file above is ``yourSubtitleManagerReference.Print(2)`` output is:

| ``oh``
| ``this is``

then, when you call ``yourSubtitleManagerReference.Print(3)`` output is:

| ``really``
| ``really``
| ``nice``

See more on :ref:`print-f`

*******************************************************

Take a look at complete :doc:`reference`

:doc:`index`
