Subtitle Tool |version| - Documentation Homepage
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   start
   writefiles
   reference

Main questions
--------------

Where am I?
^^^^^^^^^^^

You are at the official documentation page for Subtitle Tool asset for Unity3d game engine.

What is Subtitle Tool?
^^^^^^^^^^^^^^^^^^^^^^

Subtitle Tool is asset that will help you to create subtitles or closed captions for your game using text stored in .csv and .txt files. Idea is to print bunch of text on scene like narratives, conversations and tutorials with one function call.

Subtitle Tool is still in very early stage and feedback is greatly appreciated.

Can I get it?
^^^^^^^^^^^^^

Sure. It is free and you can download it `here <https://drive.google.com/open?id=0B8V2MRNFOueLMEFYWDVLaHptVVE>`_ (it will be on  Unity asset store soon!).

.. _contact:

Contact
-------

For bugs, suggestions or any kind of feedback contact me on srkyman@gmail.com
