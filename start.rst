Quick Start
===========

But of course...
----------------

...first you need to download it `here <https://drive.google.com/open?id=0B8V2MRNFOueLMEFYWDVLaHptVVE>`_ and import it in your Unity project. Assuming you did that, it is now located under *Tools/SubtitleTool*.

Overview
--------

First, check out demo scene located at *Tools/SubtitleTool/Demo/01*, I hope it is self explanatory. Hit play, follow the hints on screen and let it show you its capabilities.

*Note:*
  *If you happen to push buttons a lot for fun the SubtitleTool will misbehave.*

First look at functions
^^^^^^^^^^^^^^^^^^^^^^^

There is an example script located at *Tools/SubtitleTool/Demo/SubtitleHelper.cs*. Most important thing here for now is that you need reference to *SubtitleManager* so you can call functions on it.

SubtitleManager
^^^^^^^^^^^^^^^

That is script that you will use to configure your subtitles. There are two instances on demo scene, they are configured differently. Every SubtitlesManager can work independent on each other SubitlesManager's state. **Every SubtitleManager need SubtitlesUI in order to work.**

SubtitlesUI
^^^^^^^^^^^

You have example of it at *Tools/SubtitleTool/Resources/Prefabs/UISubtitles*. You can edit that prefab to your wish so subtitles look just as you want. **You can use only one SubtitleManager per SubtitleUI** (yeah, it sucks for now).

Where is the text?
^^^^^^^^^^^^^^^^^^

There are two .csv files located under *Tools/SubtitleTool/Demo/Resources/Subtitles* that holds text of the subtitles and if you look at any SubtitleManager on demo scene you will see that it have reference to a file there.

How to print my own subtitles?
------------------------------

Create .txt file and write **one subtitle per line** in it. You can leave empty lines for breaks. Save file, make reference to your file on your SubtitleManager. Create script and add code to it, like:

.. code-block:: csharp

    using UnityEngine;
    using SubtitleTool;

    public class QuickStart : MonoBehaviour {
      //  make sure manager is not null
      public SubtitleManager manager;

      public void PrintAll () {
        manager.PrintUntilEOF ();
      }

      public void PrintSome (){
        manager.PrintUntilEmptyRow ();
      }
    }


call them functions sometimes and you are printing from your file! Subtitle Tool will read from any text file, you have option for labeling rows, printing labeled conversations.

Subtitle Tool is still in very early stage and feedback is greatly appreciated.

Now what?
---------

Now you may want to use it, or maybe to get more details on:

:doc:`/writefiles`

:doc:`/reference`

or go to

:doc:`/index`
