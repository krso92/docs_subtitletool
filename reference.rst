Reference
=========

:doc:`index`

SubtitleManager Component
+++++++++++++++++++++++++

Input Parameters
^^^^^^^^^^^^^^^^

.. code-block:: none

  Delimiter

Delimiter is used to delimit between columns in a file. Set delimiter for subtitle file that is referenced with SubtitleManager component. Supported delimiters are: **Tab, Semicolon, Comma, Carret, Pipe**.

*******************************************************

.. code-block:: none

  SubtitlesUI

Reference to SubtitlesUI that SubtitleManager use to print subtitles on UI. *(Only UnityEngine.UI.Text is supported)*

*******************************************************

.. code-block:: none

  Print type

There are two types of print:

  **Show** that just print characters without any effects

  **Char by one** that prints one by one character until it prints subtitle
    **Print pause** option where you specify pause between printing two characters.

*******************************************************

.. code-block:: none

  Subtitle duration

There are two types of subtitle duration:

  **Fixed** meaning every subtitle will have same fixed time of visibility
    **Duration** - specify duration in seconds

  **Per character** - duration of subtitles is variable and depends on number of characters in subtitle.
    **Min duration** - duration of subtitles will never be less then specified
    **Time per char** - every character adds specified time on subtitle duration, respecting *Min duration* value

*******************************************************

.. code-block:: none

  Subtitle skipping

You can choose to enable to skip subtitles. There are two sub menus *Skip Single* and *Skip All*. You can choose one or both. There are same fields under both of sub menus:

  **enabled** - Tick this if you want to enable skipping

  **axis** - Write axis that player will have to press in order to perform skip

*******************************************************

.. code-block:: none

  Files

Here you need to make reference to file that will be used to print subtitles from. *(Only one file per SubtitleManager is supported)*

*******************************************************

Functions
^^^^^^^^^

.. _print-f:

Printing using cursor
---------------------

Whenever you use any function to print from file, cursor is set to the next row in file.

.. code-block:: csharp

  public void PrintUntilEOF(bool fromBeginning = false)

Prints the subtitles until end of file.

  **fromBegining** - If set to *true* reads subtitles from beginning, else continues where it left of

*******************************************************

.. code-block:: csharp

  public void PrintUntilEmptyRow()

Prints subtitles until empty row.

*******************************************************

.. code-block:: csharp

  public void Print(int numberOfRows, bool loopThroughFile = false)

Prints specified number of rows or until end of file.

      **numberOfRows** Number of rows to print.

      **loopThroughFile** If set to *true* loop through file. Cursor will be set to beginning when last row is printed

*******************************************************

.. code-block:: csharp

  public void PrintNext(bool loopThroughFile = false)

Prints next subtitle.
  **loopThroughFile** If set to *true* loop through file. Cursor will be set to beginning when last row is printed

*******************************************************

.. _print-f1:

Print by label
--------------

.. code-block:: csharp

  public void Print(string label, bool isConversation, Action onSubtitlesDone = null)

Print subtitle for specified label.

  **label** Label to print by

  **isConversation** If set to *true* looks for conversation label in meta column. Conversation labels start with *begin _label_* and ends with *end*

  **onSubtitlesDone** Callback that is called when subtitles are done printing or skipped.

*******************************************************

Canceling
---------

.. code-block:: csharp

  public void CancelNow()

Cancel and hide currently running subtitles immediately.

*******************************************************

.. code-block:: csharp

  public void CancelAfterCurrent()

Clear the subtitles queue.

*******************************************************

:doc:`writefiles`

:ref:`contact`
